$(document).ready(function(){

	var width = $(window).width();

    if(width > 768){
        $('.list-data').niceScroll({
            cursorborder: '1px solid transparent',
            scrollspeed: 200,
            mousescrollstep: 50,
            horizrailenabled:false
        });
    }

    $('.guest-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        autoplay:true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:4
            }
        }
    });

    $('.list-data').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        autoplay:true,
        slideTransition: 'linear',
        autoplayTimeout : 5000,
        autoplaySpeed : 5000,
        autoplayHoverPause : false,
        responsive:{
            0:{
                items:1
            },
            992:{
                items:2
            }
        }
    });

	$('.navTrigger').click(function(){
		$(this).toggleClass('active');
		$('header').find('.menu').toggleClass('mobile-active');
		$('header').toggleClass('active-head');
		$('body').toggleClass('overflow-y-hidden');
	});

    $('.close-menu').click(function(){
        $('.navTrigger').removeClass('active');
        $('header').find('.menu').removeClass('mobile-active');
        $('header').removeClass('active-head');
        $('body').removeClass('overflow-y-hidden');
    })

	if (width < 991.98){
		$('.menu').find('ul > li').has('ul').addClass('child');
		$('.menu').find('ul > li > ul > li').has('ul > li').removeClass('child');
		$('.menu').find('ul > li > ul > li').has('ul > li').addClass('child2');
	}else{
		$('.menu').find('ul > li > ul > li').has('ul > li').addClass('child');
	}

	$('.menu').find('ul > li').has('ul').ready(function(){
		$('.menu').find('ul > .child > a').removeAttr('href');
	});

	$('.menu').find('ul > li > ul > li').has('ul').ready(function(){
		$('.menu').find('ul > li > ul > .child > a').removeAttr('href');
		$('.menu').find('ul > li > ul > .child2 > a').removeAttr('href');
	});

	$('.menu').find('ul > li').has('ul').find('a').click(function(){
		$(this).next().toggleClass('active');
		$(this).parent().toggleClass('changed');
	});

	$('.menu').find('ul > li > ul > li').has('ul').find('a').click(function(){
		$(this).next().toggleClass('active-child');
	});

    var _overlay = document.getElementById('overlay');
    var _clientY = null; // remember Y position on touch start

    _overlay.addEventListener('touchstart', function (event) {
        if (event.targetTouches.length === 1) {
            // detect single touch
            _clientY = event.targetTouches[0].clientY;
        }
    }, false);

    _overlay.addEventListener('touchmove', function (event) {
        if (event.targetTouches.length === 1) {
            // detect single touch
            disableRubberBand(event);
        }
    }, false);

    function disableRubberBand(event) {
        var clientY = event.targetTouches[0].clientY - _clientY;

        if (_overlay.scrollTop === 0 && clientY > 0) {
            // element is at the top of its scroll
            event.preventDefault();
        }

        if (isOverlayTotallyScrolled() && clientY < 0) {
            //element is at the top of its scroll
            event.preventDefault();
        }
    }

    function isOverlayTotallyScrolled() {
        // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
        return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
    }

    $('#button-addon1').click(function(){
    	var inputValue = $('#input-addon1').val();
    	var newValue = parseInt(inputValue) + 1;
    	$('#input-addon1').val(newValue);
    });

    $('#button-addon2').click(function(){
    	var inputValue = $('#input-addon1').val();
    	var newValue = parseInt(inputValue) - 1;
    	$('#input-addon1').val(newValue);
    });

    $('#new-button-addon1').click(function(){
    	var inputValue = $('#new-input-addon1').val();
    	var newValue = parseInt(inputValue) + 1;
    	$('#new-input-addon1').val(newValue);
    });

    $('#new-button-addon2').click(function(){
    	var inputValue = $('#new-input-addon1').val();
    	var newValue = parseInt(inputValue) - 1;
    	$('#new-input-addon1').val(newValue);
    });

    $('.swipe-trigger').click(function(){
        $('.swipe-container').toggleClass('active');
    });

    $('.swipe-close').click(function(){
       $('.swipe-container').removeClass('active'); 
    });

    (function() {
        var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame ||
        function(callback) {
            window.setTimeout(callback, 1000 / 60);
        };
        window.requestAnimationFrame = requestAnimationFrame;
    })();


    var flakes = [],
        canvas = document.getElementById('canvas'),
        ctx = canvas.getContext('2d'),
        flakeCount = 200,
        mX = -100,
        mY = -100

        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

    function snow() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        for (var i = 0; i < flakeCount; i++) {
            var flake = flakes[i],
                x = mX,
                y = mY,
                minDist = 150,
                x2 = flake.x,
                y2 = flake.y;

            var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),
                dx = x2 - x,
                dy = y2 - y;

            if (dist < minDist) {
                var force = minDist / (dist * dist),
                    xcomp = (x - x2) / dist,
                    ycomp = (y - y2) / dist,
                    deltaV = force / 2;

                flake.velX -= deltaV * xcomp;
                flake.velY -= deltaV * ycomp;

            } else {
                flake.velX *= .98;
                if (flake.velY <= flake.speed) {
                    flake.velY = flake.speed
                }
                flake.velX += Math.cos(flake.step += .05) * flake.stepSize;
            }

            ctx.fillStyle = 'rgba(62, 88, 143,' + flake.opacity + ')';
            flake.y += flake.velY;
            flake.x += flake.velX;
                
            if (flake.y >= canvas.height || flake.y <= 0) {
                reset(flake);
            }


            if (flake.x >= canvas.width || flake.x <= 0) {
                reset(flake);
            }

            ctx.beginPath();
            ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);
            ctx.fill();
        }
        requestAnimationFrame(snow);
    };

    function reset(flake) {
        flake.x = Math.floor(Math.random() * canvas.width);
        flake.y = 0;
        flake.size = (Math.random() * 3) + 2;
        flake.speed = (Math.random() * 1) + 0.5;
        flake.velY = flake.speed;
        flake.velX = 0;
        flake.opacity = (Math.random() * 0.5) + 0.3;
    }

    function init() {
        for (var i = 0; i < flakeCount; i++) {
            var x = Math.floor(Math.random() * canvas.width),
                y = Math.floor(Math.random() * canvas.height),
                size = (Math.random() * 3) + 2,
                speed = (Math.random() * 1) + 0.5,
                opacity = (Math.random() * 0.5) + 0.3;

            flakes.push({
                speed: speed,
                velY: speed,
                velX: 0,
                x: x,
                y: y,
                size: size,
                stepSize: (Math.random()) / 30,
                step: 0,
                opacity: opacity
            });
        }

        snow();
    };

    canvas.addEventListener('mousemove', function(e) {
        mX = e.clientX,
        mY = e.clientY
    });

    window.addEventListener('resize',function(){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    })

    init();

});